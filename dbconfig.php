<?php

	$DB_HOST = 'localhost';
	$DB_USER = 'root';
	$DB_PASS = 'rootroot';
	$DB_NAME = 'vimbadmin';
	
	try{
		$DB_con = new PDO("mysql:host={$DB_HOST};port=3306;dbname={$DB_NAME}",$DB_USER,$DB_PASS);
		$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch(PDOException $e){
		echo $e->getMessage();
	}
	
