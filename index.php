<?php

	error_reporting( ~E_NOTICE ); // avoid notice
	require_once 'dbconfig.php';

	if(isset($_POST['btnsave']))
	{
		$name = $_POST['name'];
		$lastname = $_POST['lastname'];
		$username = $_POST['username'];// user name
		$username = $username."@belos.ir";
		$local_part = $_POST['lastname'];
		$password = $_POST['password'];
		$password_md5 = md5($password);
		$auth_password = $_POST['password_auth'];
		$phone = $_POST['phone'];
		$altermail = $_POST['altermail'];
		$active = 0;
		$domain = "belos.email";






		if(empty($name)){
			$errMSG = "Please enter yourname.";
		}
		else if(empty($lastname)){
			$errMSG = "Please enter Your lastname.";
		}
		else if(empty($username)){
			$errMSG = "Please enter your username.";

		}else if(empty($password)){
			$errMSG = "Please select a password";

		}else if(empty($phone)){
			$errMSG = "Please enter your phone number.";

		}else if(empty($altermail)){
			$errMSG = "Please select a recovery password.";

		}else if($password !== $auth_password){
			$errMSG = "Passwords does not matches. ";
		}



		// if no error occured, continue ....
		if(!isset($errMSG))
		{
			$stmt = $DB_con->prepare('INSERT INTO mailbox(name,lastname,username,password,phone,altermail,active,local_part,domain) VALUES(:name, :lastname, :username, :password, :phone, :altermail, 0,:localpart,:domain)');
			$stmt->bindParam(':name',$name);//local_part
			$stmt->bindParam(':lastname',$lastname);
			$stmt->bindParam(':localpart',$local_part);
			$stmt->bindParam(':username',$username);
			$stmt->bindParam(':password',$password_md5);
			$stmt->bindParam(':phone',$phone);
			$stmt->bindParam(':altermail',$altermail);
			$stmt->bindParam(':domain',$domain);

			if($stmt->execute())
			{
				$successMSG = "new record succesfully inserted ...";
				header("refresh:5;index.php"); // redirects image view page after 5 seconds.
			}
			else
			{
				$errMSG = "error while inserting....";
			}
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BelOS Registertion form</title>

<link rel="stylesheet" href="static/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="static/css/bootstrap-theme.min.css">

</head>
<body>

<div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">


    </div>
</div>

<div class="container">


	


	<?php
	if(isset($errMSG)){
			?>
            <div class="alert alert-danger">
            	<span class="glyphicon glyphicon-info-sign"></span> <strong><?php echo $errMSG; ?></strong>
            </div>
            <?php
	}
	else if(isset($successMSG)){
		?>
        <div class="alert alert-success">
              <strong><span class="glyphicon glyphicon-info-sign"></span> <?php echo $successMSG; ?></strong>
        </div>
        <?php
	}
	?>

<form method="post" enctype="multipart/form-data" class="form-horizontal">

	<table class="table table-bordered table-responsive">

    <tr>
    	<td><label class="control-label">Name:</label></td>
        <td><input class="form-control" type="text" name="name" placeholder="Enter your name" value="<?php echo $name; ?>" /></td>
    </tr>

    <tr>
    	<td><label class="control-label">Lastname</label></td>
        <td><input class="form-control" type="text" name="lastname" placeholder="Enter your lastname" value="<?php echo $lastname; ?>" /></td>
    </tr>
        <tr>
    	<td><label class="control-label">username: </label></td>
        <td><input class="form-control" type="text" name="username" placeholder="Select your username" value="<?php echo $username; ?>" /></td>
    </tr>
        <tr>
    	<td><label class="control-label">password: </label></td>
        <td><input class="form-control" id="passwordvalue" type="password" name="password" placeholder="Select password" value="<?php echo $password; ?>" /></td>
    </tr>
        <tr>
    	<td>
    	<label class="control-label">Password(again): </label>
 	<p id="pass_ready">
    	</td>
        <td><input class="form-control" id="passwordauth" type="password" name="password_auth" placeholder="Enter your password again" value="<?php echo $password; ?>" /></td>
    </tr>
        <tr>
    	<td><label class="control-label">Phone: </label></td>
        <td><input class="form-control" type="text" name="phone" placeholder="Your phone number" value="<?php echo $phone; ?>" /></td>
    </tr>
        <tr>
    	<td><label class="control-label">recovery Email: </label></td>
        <td><input class="form-control" type="email" name="altermail" placeholder="recovery email" value="<?php echo $altermail; ?>" /></td>
    </tr>


    <tr>
        <td colspan="2"><button type="submit" name="btnsave" class="btn btn-default" >
        <span class="glyphicon glyphicon-save"></span> &nbsp; save
        </button>
        </td>
    </tr>

    </table>

</form>


</div>






<!-- Latest compiled and minified JavaScript -->
<script src="bootstrap/js/bootstrap.min.js"></script>

</body>
</html>
